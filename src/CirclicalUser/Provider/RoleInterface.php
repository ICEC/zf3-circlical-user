<?php

namespace CirclicalUser\Provider;

/**
 * Interface RoleInterface
 *
 * A user role, with a parent.
 *
 * @package CirclicalUser\Provider
 */
interface RoleInterface
{
    public function getId() ;

    public function getName() ;

    public function getParent();
}