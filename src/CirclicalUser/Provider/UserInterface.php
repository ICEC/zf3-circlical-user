<?php

namespace CirclicalUser\Provider;

interface UserInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getRoles();

    /**
     * @return mixed
     */
    public function getEmail();

    /**
     *  We need this for the JWT token
     * @return mixed
     */
    public function getVoornaam();

    /**
     * @param RoleInterface $role
     * @return mixed
     */
    public function addRole(RoleInterface $role);
}